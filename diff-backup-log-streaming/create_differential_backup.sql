USE [msdb]
GO

DECLARE @DatabaseName varchar(100)
SET @DatabaseName='testdb'

DECLARE @OwnerLogin varchar(100)
SET @OwnerLogin='WIN-T8A6615TGBJ\Administrator'

DECLARE @JobName varchar(100)
SET @JobName=(SELECT '' + @DatabaseName + '-differential-backup-job')

DECLARE @ScheduleUID varchar(100)
SET @ScheduleUID=(SELECT NEWID())

DECLARE @DifferentialBackupDirectory varchar(100)
SET @DifferentialBackupDirectory=(SELECT 'S:\BACKUP\'+ @DatabaseName + '\Differentials\')

DECLARE @CreateDailyDirCommand varchar(500)
SET @CreateDailyDirCommand=N'$Day=Get-Date -format yyyyMMdd
$SavePath= "'+ @DifferentialBackupDirectory +'\{0}" -f $Day
New-Item -ItemType "directory" -Force -Path $SavePath'

DECLARE @CreateDiffBackupCommand varchar(500)
SET @CreateDiffBackupCommand=N'DECLARE @DateNumber varchar(60)=(SELECT REPLACE(CONVERT(VARCHAR(11),GETDATE(),8), '':'', ''''))
DECLARE @LocationToSave varchar(600)=(SELECT ''' + @DifferentialBackupDirectory + ''' + @DateNumber +''.bak'')
DECLARE @BackupName varchar(100)=(SELECT ''' + @DatabaseName + ''' + ''-Differential Database Backup'')
BACKUP DATABASE ' + @DatabaseName + ' TO DISK =@LocationToSave
WITH  DIFFERENTIAL, 
FORMAT, 
NAME =@BackupName, 
SKIP,
 NOREWIND, 
 NOUNLOAD, 
  STATS = 10,
  COMPRESSION
GO
'

-- The number of minutes between differential backup job runs
DECLARE @NumMinutesInterval int
SET @NumMinutesInterval=1


BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Differential Backups' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Differential Backups'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@category_name=N'Differential Backups', 
		@owner_login_name=@OwnerLogin, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Daily Differential',
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@CreateDiffBackupCommand, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Differential Backups', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=2, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160909, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=@ScheduleUID
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
-- When done enable the job
EXEC msdb.dbo.sp_update_job @job_name=@JobName,@enabled=1
GO
