# Diff Backup Process
Differential backups are a way to start with an initial backup and then take smaller backup of the differences from the intial backup.  This allows you to restore the original DB using a full backup and then create differential backps to get up to the desired time.  For us, we will do weekly full backups and daily differential backups.

## Step 1 - Full Backup
First off you need to create a full backup so you have a place to start.  Here's an example:

```sql
BACKUP DATABASE [testdb] TO  DISK = N'S:\BACKUP\testdb\testdb.bak' WITH NOFORMAT, NOINIT, COMPRESSION, NAME = N'testdb-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO
```

This will create a compressed file backup of the database at the ```DISK``` location. Feel free to name it whatever you'd like.

## Step 2 - Create Differential Backups
The [create_differential_backup](create_differential_backup.sql) script creates a job that generates transction logs for your database.  There are parameters at the top of the file that are hopefully named sanely that are used in the below snippets.

There are a few variables at the top of the file that should help make this extensible.  Here's a table with details:

| Variable                 | Description                                                       | Default             |
| ------------------------ | ----------------------------------------------------------------- | ------------------- |
| DatabaseName             | The database to perform the backups against                       | testdb        |
| OwnerLogin               | The account that should be used to perform the script             | Administrator |
| JobName                  | The name to give the differential backup job                    | `DatabaseName`-backup-job |
| LogStepName              | The name to give the step that runs the backup command            | `DatabaseName`-backup-log-step |
| DifferentialBackupDirectory  | The directory where differntial backup will be stored for this DB   | S:\ `DatabaseName` \Differentials\ |
| ScheduleUID              | The UID to give the schedule when created                         | `NEWID()` |
| CreateDailyDirCommand    | The command that creates the directory structure using powershell | `in code` |
| CreateDiffBackupCommand  | The command that creates the transaction log                      | `in code` |
| NumMinutesInterval       | The number of minutes between transaction log backup job runs     | 1 |

# TODO
- [] Update the time in the backup script to be once per day not per minute
