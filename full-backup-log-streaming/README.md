# Full Backup with Log Streaming
Full backup log streaming is a way to manage your database backups by completely restoring the database from a full backup and then restoring (playing back) the individual transactions from a log until the last point in time.

# Prerequisites
These instructions and documentation assume that you have created a database with the script in [this snippet](https://bitbucket.org/snippets/taelliot/korry).

# Full Backup and Setting Up Transaction Logs
## Step 1 - Full Backup
First off you need to create a full backup so you have a place to start.  Here's an example:

```sql
BACKUP DATABASE [testdb] TO  DISK = N'S:\BACKUP\testdb\testdb.bak' WITH NOFORMAT, NOINIT, COMPRESSION, NAME = N'testdb-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO
```

This will create a compressed file backup of the database at the ```DISK``` location. Feel free to name it whatever you'd like.

## Step 2 - Create Transaction Logs
Transactions logs say what happened in the DB since the last ones were taken.  The [backup-database-trx-logs.sql](backup_database_trx_logs.sql) script creates a job that generates transction logs for your database.  There are parameters at the top of the file that are hopefully named sanely that are used in the below snippets.

There are a few variables at the top of the file that should help make this extensible.  Here's a table with details:

| Variable                 | Description                                                       | Default             |
| ------------------------ | ----------------------------------------------------------------- | ------------------- |
| DatabaseName             | The database to perform the backups against                       | testdb        |
| OwnerLogin               | The account that should be used to perform the script             | Administrator |
| JobName                  | The name to give the transction log backup job                    | `DatabaseName`-backup-job |
| LogStepName              | The name to give the step that runs the backup command            | `DatabaseName`-backup-log-step |
| TransacctionLogDirectory | The directory where transaction logs will be stored for this DB   | S:\ `DatabaseName`-TransactionLogs |
| ScheduleUID              | The UID to give the schedule when created                         | `NEWID()` |
| CreateDailyDirCommand    | The command that creates the directory structure using powershell | `in code` |
| CreateTrxLogCommand      | The command that creates the transaction log                      | `in code` |
| NumMinutesInterval       | The number of minutes between transaction log backup job runs     | 1 |

Here are the highlights:

### Create Daily Directory Task
This simple powershell script creates the daily folder directory for the database. It uses the ```@TransactionLogDirectory``` as its base and then adds in the folder for today.  It says success even if the folder exists.

```ps
SET @CreateDailyDirCommand=N'$Day=Get-Date -format yyyyMMdd
$SavePath= "'+ @TransactionLogDirectory +'\{0}" -f $Day
New-Item -ItemType "directory" -Force -Path $SavePath'
```

### Create Transaction Log Task

This is run at the ```@NumMinutesInterval``` specified in the script.  Looking at the script it creates a transaction log backup to the ```@TransactionLogDirectory``` folder in a subfolder that has the days date. The actual transaction log backup file is in the format ```hhmmss.trn```.  Here's an example:  ```S:\BACKUP\TestDB\TransactionLogs\20160908\120000.trn```

```sql
SET @CreateTrxLogCommand=N'declare @filename varchar(60)
set @filename=(SELECT ''' + @TransactionLogDirectory + ''' + ''\'' + CONVERT(VARCHAR(11),GETDATE(),112) + ''\'' + REPLACE(CONVERT(VARCHAR(11),GETDATE(),8) + ''.TRN'', '':'', ''''))
BACKUP LOG ' + @DatabaseName + '
TO DISK = @filename
GO'
```

# Full Restore and Stream Transaction Logs
## Step 1 - Restore The DB
You will need to get the full database backup onto your machine to start and restore it to a replica database you created.  Once you have the backup it here's an example of what to run:

```sql
CREATE DATABASE [testdb-replica]
```

```sql
RESTORE DATABASE [testdb] FROM DISK=N'S:\FullBackup\testdb.bak' WITH REPLACE, STANDBY='S:\DATA\testdb-replica.mdf', NOUNLOAD,  STATS=10
```

## Step 2 - Restore the Logs
For each log file in the `TransactionLogDirectory` you will have to restore the file.  Here's what that command looks like:

```sql
RESTORE LOG [testdb-replica] FROM DISK='@TransactionLogDirectory\20160908\200200.trn' WITH STANDBY='S:\DATA\testdb-replica.mdf'
```

## Powershell To The Rescue
This above process is a little tedious so there is a simple powershell [script](../cmd/restore_from_logs.ps1) that makes it a little easier.

# Synchronizing the Secondary with a Master on Different Hosts
To do this we will be [pushing up](../cmd/upload_folder_to_s3.ps1) the big backup and all the transaction logs to S3 from the master.  The secondary will then [pull down](../cmd/pull_from_s3.ps1) those logs and do the above processes to restore them.

# TODO
- [] Update the time in the backup script to be once per day not per minute
