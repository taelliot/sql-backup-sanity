USE [master]
GO
DECLARE @DatabaseName varchar(100)
SET @DatabaseName='testdb'

DECLARE @OwnerLogin varchar(100)
SET @OwnerLogin='Administrator'

DECLARE @JobName varchar(100)
SET @JobName=(SELECT '' + @DatabaseName + '-backup-job')
DECLARE @LogStepName varchar(100)
SET @LogStepName=(SELECT '' + @DatabaseName + '-backup-log-step')

DECLARE @ScheduleUID varchar(100)
SET @ScheduleUID=(SELECT NEWID())

DECLARE @TransactionLogDirectory varchar(100)
SET @TransactionLogDirectory=(SELECT 'S:\BACKUP\'+ @DatabaseName + '\TransactionLogs')

DECLARE @CreateDailyDirCommand varchar(500)
SET @CreateDailyDirCommand=N'$Day=Get-Date -format yyyyMMdd
$SavePath= "'+ @TransactionLogDirectory +'\{0}" -f $Day
New-Item -ItemType "directory" -Force -Path $SavePath'

DECLARE @CreateTrxLogCommand varchar(500)
SET @CreateTrxLogCommand=N'declare @filename varchar(60)
set @filename=(SELECT ''' + @TransactionLogDirectory + ''' + ''\'' + CONVERT(VARCHAR(11),GETDATE(),112) + ''\'' + REPLACE(CONVERT(VARCHAR(11),GETDATE(),8) + ''.TRN'', '':'', ''''))
BACKUP LOG ' + @DatabaseName + '
TO DISK = @filename
GO'

-- The number of minutes between transaction log back job runs
DECLARE @NumMinutesInterval int
SET @NumMinutesInterval=1


BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Transactional Backups' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Transactional Backups'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'This job performs transactional backups of the database.', 
		@category_name=N'Transactional Backups', 
		@owner_login_name=@OwnerLogin, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

-- This job step creates the daily directory if it doesn't exist
EXEC msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Daily Directory', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_fail_action=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=@CreateDailyDirCommand,
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

-- Make the job that actually performs the log writing
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@LogStepName, 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@CreateTrxLogCommand, 
		@database_name=@DatabaseName,
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

-- Add a schedule to the jobs
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=@NumMinutesInterval, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160902, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=@ScheduleUID
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
-- When done enable the job
EXEC msdb.dbo.sp_update_job @job_name=@JobName,@enabled=1
GO
