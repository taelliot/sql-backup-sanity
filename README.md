# SQL Backup Sanity
The historical process for getting the desired backups of databases onto the dev and qa database instances historically has been a manual process that takes time.  The code in this repo is aimed at trying to make this process less painless.  There are 2 appraches listed below.

# Approaches 
## [Full Backup Log Streaming](full-backup-log-streaming/README.md)
## [Differential Backup Log Streaming](diff-backup-log-streaming/README.md)
## [Powershell Helpers](powershell/README.md)
