:: this uploads all of the transaction logs to a defined s3 bucket. 
:: the instance needs to have the s3 IAM roles necessary to perform bucket create and upload

PowerShell.exe -ExecutionPolicy Bypass ^
     -Command "& 'S:\Scripts\upload_folder_to_s3.ps1' " ^
     "-diffBackupLocation 'S:\BACKUP\testdb\TransactionLogs\20160913' " ^
     "-s3BucketName 'testdb-trx-logs' " ^
     "-logFilePath 'S:\Scripts\upload_transaction_logs_to_s3_log.log' "

