param([String]$folderToUpload="notset",
      [String]$s3BucketName="notset",
      [String]$logFilePath="upload_to_s3_results.log"
)

function Write-Log
{
  param([String]$message)
  $logDate = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
  $logMessage = "{0} - {1}" -f $logDate,$message
  $logMessage | Out-File $logFilePath -Append
}

$bucketName=$(New-S3Bucket -BucketName $s3BucketName -Region us-east-1).BucketName
$message= "Created bucket {0}" -f $bucketName
Write-Log -message $message

$filesToUpload=(dir $folderToUpload).Name

foreach ($fileToUploadName in $filesToUpload) { 
    $fileToUpload="{0}\{1}" -f $folderToUpload, $fileToUploadName
    $s3FileRoot=Split-Path (Split-Path $fileToUpload) -Leaf
    $s3FilePath="{0}\{1}" -f $s3FileRoot,$fileToUploadName

    $logMessage="Uploading {0} to s3 bucket {1}" -f $fileToUpload,$s3BucketName
    Write-Log -message $logMessage 

    Write-S3Object -BucketName $s3BucketName -File $fileToUpload -Key $s3FilePath -CannedACLName authenticated-read
    $logMessage="Uploaded {0} to s3 bucket {1}" -f $fileToUpload,$s3BucketName
    Write-Log -message $logMessage
}

Write-Log -message "Done!"

# Remove-S3Bucket -BucketName $s3BucketName
