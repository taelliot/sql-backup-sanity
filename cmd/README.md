# Powershell
The following are brief descriptions of powershell scripts that help make backups possible.

## [upload_folder_to_s3](upload_folder_to_s3.ps1)
This script creates a bucket with the name of the last part of the folder and then uploads the contents of the directory to the bucket.

## [restore_full_backup](restore_full_backup.ps1)
Restores a full database from backup.

## [restore_from_diff](restore_from_diff.ps1)
Restores a database from replica and then restores the given backups.

## [restore_from_logs](restore_from_logs.ps1)
This restores a database from transaction logs.

## [set_credentials](set_credentials.ps1)
The profile that gets put into the powershell profile to interact with amazon.  This is a thing we can use if we need to. For now the [instance profile](../examples/s3_instance_policy.json) is used to give the machine the ability to control S3 buckets.  We should make this more granular than "admin".

## [profile](profile.ps1)
This loads the SqlServer snapins needed to create and run the various powershell scripts to restore things.

## [create_upload_transaction_logs_to_s3_service](create_upload_transaction_logs_to_s3_service.ps1)
This script makes a service that uploads logs in the given directory to s3.  It leverages the [upload_folder_to_s3](upload_folder_to_s3.ps1) to upload the contents of a folder to s3.

## [pull_transaction_logs_from_s3_service](pull_transaction_logs_from_s3_service.ps1)
This script makes a service that pulls down transaction logs from s3. It leverages the [pull_from_s3](pull_from_s3.ps1) script to pull down the contents of a bucket and grab only files it doesn't already have.
