param([String]$s3Bucket="notset",
      [String]$saveLocation="notset",
      [STring]$logFilePath="notset"
)

function Write-Log
{
   param([String]$message)
  $logDate = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
  $logMessage = "{0} - {1}" -f $logDate,$message
  $logMessage | Out-File $logFilePath -Append
}


$message="Pulling objets from {0}" -f $s3Bucket
Write-Log -message $message
$S3Objects = Get-S3Object -bucketname $s3Bucket
$FilesOfInterest = $S3objects | Select Key 

$ListOfCopiedFiles= @()

foreach ($S3File in $FilesOfInterest ) {
    
    $FileWeWant = $saveLocation + "\" + $S3File.Key
    $LocalExists = Test-Path ($FileWeWant)

    if ($LocalExists -eq $false) {
        $message="Getting copy of {0}..." -f $S3File.Key
        Write-Log $message
        Copy-S3Object -BucketName $s3Bucket -Key $S3File.Key -LocalFile $FileWeWant
        $ListOfCopiedFiles = $ListOfCopiedFiles + $S3file.Key
        $message="Saved copy of {0}..." -f $S3File.Key
        Write-Log $message
    }
}

Write-Log "Done!"
