param([String]$replicaDBName="notset",
      [String]$fullBackupLocation="notset",
      [String]$replicaDBLocation="notset",
      [String]$replicaDBLogLocation="notset",
      [String]$undoLocation="notset",
      [String]$transactionLogDirectory="notset"
)

$makeReplicaDB="BEGIN TRY
RESTORE DATABASE [{0}] FROM  DISK = '{1}' 
    WITH REPLACE,
    STANDBY='{2}',
    FILE = 1,  
    MOVE N'testdb' TO '{3}',
    MOVE N'testdb_log' TO N'{4}',
    NOUNLOAD,  
    STATS = 10
print 'Restored DB {0}'
END TRY 
BEGIN CATCH
  print 'DB {0} Already exists'
END CATCH" -f $replicaDBName,$fullBackupLocation,$undoLocation,$replicaDBLocation,$replicaDBLogLocation

Write-Host ("Creating replica db {0}..." -f $replicaDBName)
Invoke-Sqlcmd -Query $makeReplicaDB

$Day=Get-Date -format yyyyMMdd
$LogPath="{0}\{1}" -f $transactionLogDirectory,$Day

$trnFiles=(dir $LogPath).Name
foreach ($trxLog in $trnFiles) {
    $logFile="{0}\{1}" -f $LogPath,$trxLog
    Write-Host("Restoring transaction log {0}" -f $logFile)
    $query="RESTORE LOG [{0}] FROM DISK='{1}' WITH STANDBY='{2}'" -f $replicaDBName,$logFile,$undoLocation
    Invoke-Sqlcmd -Query $query
}

# optionally restore to full mode or leave in standby
# $finalRestore="RESTORE DATABASE [{0}] WITH RECOVERY" -f $replicaDBName
# Invoke-Sqlcmd -Query $finalRestore
