:: this uploads all of the transaction logs to a defined s3 bucket. 
:: the instance needs to have the s3 IAM roles necessary to perform bucket create and upload

PowerShell.exe -ExecutionPolicy Bypass ^
     -Command "& 'C:\Users\Administrator\Documents\pull_from_s3.ps1' " ^
     "-saveLocation 'S:\BACKUP\testdb\TransactionLogs\sync' " ^
     "-s3Bucket 'testdb-trx-logs' "^
     "-logFilePath 'S:\Scripts\pull_transaction_logs_from_s3_log.log' "
