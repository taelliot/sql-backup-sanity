:: this invokes a restore of all the transaction logs in the testdb transaction logs directory
:: this uses the restore-from-logs.ps1 powershell script in the powershell directory

PowerShell.exe -ExecutionPolicy Bypass ^
     -Command "& 'restore_logs.ps1' " ^
     "-replicaDBName 'testdb-replica' " ^
     "-fullBackupLocation 'S:\BACKUP\testdb\testdb.bak' " ^
     "-replicaDBLocation 'S:\Data\testdb-replica\testdb-replica.mdf' " ^
     "-replicaDBLogLocation 'S:\Data\testdb-replica\testdb-replica_1.ldf' " ^
     "-undoLocation 'S:\Data\testdb-replica\testdb-replica-undo.mdf' " ^
     "-transactionLogDirectory 'S:\BACKUP\testdb\TransactionLogs' "

