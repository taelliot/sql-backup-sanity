:: this invokes a restore of all the differentials in the testdb differential directory
:: this uses the restore-from-diff.ps1 powershell script in the powershell directory

PowerShell.exe -ExecutionPolicy Bypass ^
     -Command "& 'restore_from_diff.ps1' " ^
     "-originalDBName 'testdb' " ^
     "-originalDBLogName 'testdb_log' " ^
     "-replicaDBName 'testdb-replica' " ^
     "-diffBackupLocation 'S:\BACKUP\testdb\Differentials' " ^
     "-undoLocation 'S:\Data\testdb-replica\testdb-replica-undo.mdf' " ^
     "-replicaDBLocation 'S:\Data\testdb-replica\testdb-replica.mdf' " ^
     "-replicaDBLogLocation 'S:\Data\testdb-replica\testdb-replica_1.ldf' "

