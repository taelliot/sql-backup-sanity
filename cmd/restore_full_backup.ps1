param([String]$replicaDB="notset")
param([String]$roriginalBackup="notset")
param([String]$replicaOriginal="notset")

$restoreQuery="RESTORE DATABASE {0} FROM DISK=N'{1}' WITH REPLACE, STANDBY='{2}', NOUNLOAD,  STATS=10" -f $replicaDB,$originalBackup,$replicaOriginal

Invoke-Sqlcmd -Query $restoreQuery
