param([String]$originalDBName="notset", # testdb
      [String]$originalDBLogName="notset", # testdb_log
      [String]$replicaDBName="notset", # testdb-replica
      [String]$fullBackupLocation="notset", # 'S:\BACKUP\testdb\testdb.bak'
      [String]$diffBackupLocation="notset", # 'S:\BACKUP\testdb\Differentials'
      [String]$undoLocation="notset", #'S:\Data\testdb-replica\testdb-replica-undo.mdf'
      [String]$replicaDBLocation="notset", #'S:\Data\testdb-replica\testdb-replica.mdf'
      [String]$replicaDBLogLocation="notset" #N'S:\Data\testdb-replica\testdb-replica_1.ldf'
)  

$makeReplicaDB="BEGIN TRY
RESTORE DATABASE [{0}] FROM  DISK = '{1}' 
    WITH REPLACE,
    STANDBY='{2}',
    FILE = 1,  
    MOVE N'testdb' TO '{3}',
    MOVE N'testdb_log' TO N'{4}',
    NOUNLOAD,  
    STATS = 10
print 'Restored DB {0}'
END TRY 
BEGIN CATCH
  print 'DB {0} Already exists'
END CATCH" -f $replicaDBName,$fullBackupLocation,$undoLocation,$replicaDBLocation,$replicaDBLogLocation

Write-Host ("Creating replica db {0}..." -f $replicaDBName)
Invoke-Sqlcmd -Query $makeReplicaDB

$diffBackups=(dir $diffBackupLocation).Name
$diffFiles=(dir $diffBackupLocation).Name

foreach ($diffFileName in $diffFiles) {
    $diffFile="{0}\{1}" -f $diffBackupLocation, $diffFileName
    Write-Host ("Restoring {0}" -f $diffFile)

    $query="RESTORE DATABASE [{0}] FROM  DISK ='{1}'
        WITH REPLACE, 
        STANDBY='{2}', FILE = 1, 
        MOVE '{3}' TO '{4}',
        MOVE '{5}' TO '{6}',
        NOUNLOAD,  STATS = 10" -f $replicaDBName,$diffFile,$undoLocation,$originalDBName,$replicaDBLocation,$originalDBLogName,$replicaDBLogLocation

    Invoke-Sqlcmd -Query $query
}

# optionally restore to full mode or leave in standby
# $finalRestore="RESTORE DATABASE [{0}] WITH RECOVERY" -f $replicaDBName
# Invoke-Sqlcmd -Query $finalRestore
